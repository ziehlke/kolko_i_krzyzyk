import org.junit.Assert;
import org.junit.Test;

public class QueueTurnManagerTest {


    @Test
    public void makeTurn_defaultConstructor_01_on01putX() {

        MapXO mapXO = new MapXO();
        QueueTurnManager queueTurnManager = new QueueTurnManager(mapXO);

        queueTurnManager.makeTurn(0, 1);


        Assert.assertEquals(MapValues.X, (MapValues) mapXO.getMapXO()[0][1]);
    }


    @Test
    public void makeTurnTwice_parametrizedConstructor_22_on22expectO() {
        MapXO mapXO = new MapXO();
        QueueTurnManager queueTurnManager = new QueueTurnManager(mapXO);

        queueTurnManager.makeTurn(2, 2);
        queueTurnManager.makeTurn(2, 2);

        Assert.assertEquals(MapValues.O, (MapValues) mapXO.getMapXO()[2][2]);
    }



    @Test
    public void checkWin_defaultConstructor_3inRow_true(){

        MapXO mapXO = new MapXO();
        QueueTurnManager queueTurnManager = new QueueTurnManager(mapXO);

        queueTurnManager.makeTurn(0,0);
        queueTurnManager.makeTurn(1,1);
        queueTurnManager.makeTurn(0,1);
        queueTurnManager.makeTurn(2,1);
        queueTurnManager.makeTurn(0,2);


        boolean condition = queueTurnManager.checkWin();

        Assert.assertTrue(condition);

    }



}
