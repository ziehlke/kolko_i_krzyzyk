public class MapXO {

    private Object[][] mapXO;

    public MapXO() {

        this(3);
    }

    public MapXO(int size) {
        this.mapXO = new Object[size][size];

        for (int i=0; i < mapXO.length; i++) {
            for (int j=0; j < mapXO[i].length; j++) {
                 mapXO[i][j] = MapValues.NOT_SET;
            }
        }


    }

    public Object[][] getMapXO() {
        return mapXO;
    }

    public void setValue(int x, int y) {
        setValue(x,y, MapValues.X);

    }

    public void setValue(int x, int y, MapValues value) {
        this.mapXO[x][y] = value;
    }
}
