import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class XOMapTest {


    private static MapXO mapXO;

    @Before
    public void setUp() {

        this.mapXO = new MapXO();

    }


    @Test
    public void getMap_defaultConstructorSize3x3() {

        Object[][] expected = new Object[3][3];

        MapXO mapXO = new MapXO();
        mapXO.getMapXO();


        int actualX = mapXO.getMapXO().length;
        int actualY = mapXO.getMapXO()[0].length;

        Assert.assertEquals(3, actualX);
        Assert.assertEquals(3, actualY);

    }


    @Test
    public void getMapXO_parametrizedConstructor4_MapXOLenght4x4() {

        int expected = 4;

        MapXO mapXO = new MapXO(4);
        int actualX = mapXO.getMapXO().length;
        int actualY = mapXO.getMapXO()[0].length;

        Assert.assertEquals(expected, actualX);
        Assert.assertEquals(expected, actualY);

    }


    @Test(expected = IllegalArgumentException.class)
    public void getMapXO_parametrizedConstructorMapSize2_IllegalArgumentException() {

        MapXO mapXO = new MapXO(2);
        mapXO.getMapXO();

    }

    @Test
    public void setValue_defaultConstructor_11_putX_on11ExpectX() {

        MapValues expected = MapValues.X;
        mapXO.setValue(1, 1);


        MapValues actual = (MapValues) mapXO.getMapXO()[1][1];

        Assert.assertEquals(expected, actual);
    }


    @Test
    public void setValue_defaultConstructor_02_putO_on02ExpectO() {
        mapXO.setValue(0, 2, MapValues.O);

        Assert.assertEquals(MapValues.O, (MapValues) mapXO.getMapXO()[0][2]);
    }


    @Test
    public void getMapXO_defaultConstructor_allDataNOTSET() {

        for (int i = 0; i < mapXO.getMapXO().length; i++) {
            for (int j = 0; j < mapXO.getMapXO()[i].length; j++) {
                Assert.assertEquals(MapValues.NOT_SET, (MapValues) mapXO.getMapXO()[i][j]);
            }
        }


        for (int i = 0; i < mapXO.getMapXO().length; i++) {
            Assert.assertArrayEquals(
                    new MapValues[]{MapValues.NOT_SET,MapValues.NOT_SET,MapValues.NOT_SET} ,
                    mapXO.getMapXO()[i]
            );
        }

    }


}
