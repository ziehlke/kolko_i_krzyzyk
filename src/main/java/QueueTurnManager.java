public class QueueTurnManager {

    private final MapXO mapXO;
    private MapValues current;
    private MapValues old;

    public QueueTurnManager(MapXO mapXO) {
        this.mapXO = mapXO;
        this.current = MapValues.X;
        this.old = MapValues.X;

    }


    public void makeTurn(int x, int y) {
        mapXO.setValue(x, y, current);
        old = current;
        if (current == MapValues.X) current = MapValues.O;
        else current = MapValues.X;
    }


    public boolean checkWin() {

        for (int i=0; i < mapXO.getMapXO().length; i++) {
            for (int j=0; j < mapXO.getMapXO()[i].length; j++) {
                if (old.equals(mapXO.getMapXO()[i][j])) {return true;};
            }


        }


        return false;
    }
}
